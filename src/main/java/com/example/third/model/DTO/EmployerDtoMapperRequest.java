package com.example.third.model.DTO;

import com.example.third.model.Customer;
import com.example.third.model.CustomerDtoRequest;
import com.example.third.model.Employer;
import com.example.third.model.EmployerDtoRequest;
import com.example.third.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoMapperRequest extends DtoMapperFacade<Employer, EmployerDtoRequest> {

    public EmployerDtoMapperRequest() {
        super(Employer.class, EmployerDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Employer entity, EmployerDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAddress(dto.getAddress());
    }
}
