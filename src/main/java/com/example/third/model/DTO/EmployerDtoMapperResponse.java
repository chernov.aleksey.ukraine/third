package com.example.third.model.DTO;

import com.example.third.model.*;
import com.example.third.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoMapperResponse extends DtoMapperFacade<Employer, EmployerDtoResponse> {
    public EmployerDtoMapperResponse() {
        super(Employer.class, EmployerDtoResponse.class);
    }

    @Override
    protected void decorateDto(EmployerDtoResponse dto, Employer entity) {
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAddress(entity.getAddress());
//        dto.setCustomers(entity.getCustomers());
    }
}
