package com.example.third.model.DTO;

import com.example.third.model.Account;
import com.example.third.model.AccountDtoRequest;
import com.example.third.model.Customer;
import com.example.third.model.CustomerDtoRequest;
import com.example.third.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class AccountDtoMapperRequest extends DtoMapperFacade<Account, AccountDtoRequest> {

    public AccountDtoMapperRequest() {
        super(Account.class, AccountDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Account entity, AccountDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setNumber(dto.getNumber());
        entity.setCurrency(dto.getCurrency());
        entity.setBalance(dto.getBalance());
    }
}
