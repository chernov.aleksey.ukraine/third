package com.example.third.Dao;

import com.example.third.model.Employer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployerJpaRepository extends JpaRepository<Employer, Long> {

}
