
INSERT INTO public.customers (name, email, age, phone, password) VALUES
    ('Ivan Petrenko', 'ivanpetrenko@mail.com', 22, '+38067123456789', 'ivanPetrenko'),
    ('Oksana Petrenko', 'oksanapetrenko@mail.com', 21, '+38067123456789', 'oksanaPetrenko'),
    ('Sergiy Stepanenko', 'sergiystepanenko@mail.com', 24, '+38067123456789', 'sergiyStepanenko'),
    ('Galyna Stepanenko', 'galynastepanenko@mail.com', 23, '+38067123456789', 'galynaStepanenko'),
    ('Petro Ivanenko', 'petroivanenko@mail.com', 23, '+38067123456789', 'petroIvanenko'),
    ('Natalya Ivanenko', 'natalyaivanenko@mail.com', 22, '+38067123456789', 'natalyaIvanenko'),
    ('Stepan Vasylenko', 'stepanvasylenko@gmail.com', 21, '+38067123456789', 'stepanVasylenko'),
    ('Maryna Vasylenko', 'marynavasylenko@gmail.com', 20, '+38067123456789', 'marynaVasylenko'),
    ('Oleksiy Chernenko', 'oleksiychernenko@gmail.com', 25, '+38067123456789', 'oleksiyChernenko'),
    ('Kateryna Chernenko', 'katerynachernenko@gmail.com', 24, '+38067123456789', 'katerynaChernenko'),
    ('Sashko Gaplychenko', 'sashkogaplychenko@gmail.com', 22, '+38067123456789', 'sashkoGaplychenko'),
    ('Mariya Gaplychenko', 'mariyagaplychenko@gmail.com', 21, '+38067123456789', 'mariyaGaplychenko'),
    ('Ihor Serhienko', 'ihorserhienko@gmail.com', 25, '+38067123456789', 'ihorSerhienko');


INSERT INTO accounts
    (number, currency, balance, customer_id)
    VALUES
    ('82560eb9-c8f9-4be9-aaef-17fd129cec6e', 'UAH', 0,1),
    ('f02683e2-519e-487b-b855-5286182ad1d4', 'USD', 0,2),
    ('6295e6db-667e-4d1b-adc1-dce573b01942', 'UAH', 0,2),
    ('477b0882-84a5-41c3-87d8-a3b26cb7278f', 'EUR', 0,3),
    ('45d91b68-a4dd-4de3-88dd-13e106b12a0c', 'EUR', 0,5),
    ('4e204fcd-d296-483c-964c-74d0be4130bb', 'CHF', 0,6),
    ('9db430b11-6b5c-4d63-9663-4f59974ea04', 'USD', 0,7),
    ('6e49c4c9-c93a-48af-a37c-0af6d1db9d7d', 'UAH', 0,7),
    ('4f3d9294-a79e-48b5-8109-c934727f8edf', 'UAH', 0,9);

INSERT INTO employers
    (name, address)
    VALUES
    ('Google', 'us'),
    ('Microsoft', 'us'),
    ('Boston Dynamics', 'us');

INSERT INTO customerEmployment
    (customer_id, employer_id)
    VALUES
    (1, 1),
    (2, 3),
    (3, 1),
    (3, 2),
    (4, 2),
    (5, 3),
    (7, 1),
    (8, 2),
    (9, 2),
    (9, 3),
    (10, 1);


--926d32f1-e6cf-4690-b4eb-a00f2d3dde7d
--b683a48f-9992-44d4-bff2-407d39fb31d4
--9ba85a47-48ef-4cff-8d86-845a8fd0eb4c
--c51c2ed6-5a90-4777-9380-215e93405919
--ea8013e6-c6af-469d-9e6b-19b69eb70e02
--3a922ddc-9724-40cc-9a15-c506c7759bef
--d2c969b6-b05a-4b21-a566-279082277bf7
--ced5f089-cc33-4e2a-bf7e-d9296f23dbdd
--9c75eec6-2e0f-45a0-a29f-6b0d75429903
--3ba744ba-3ee7-4301-9943-25590cfc9771
--b9b6c0ff-1ff7-4968-a1ef-0e3fac0ff6ed